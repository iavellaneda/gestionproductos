/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;
import java.sql.*;
        
/**
 *
 * @author Ignacio Avellaneda <ignacioavellaneda@alu.frt.utn.edu.ar>
 */
public class DBConn {
    public Connection getConnection(){
        Connection dbconn = null;
        try {
            String dbPath = System.getProperty("user.dir");
            String url = "jdbc:sqlite:" + dbPath + "/gestionprod.db";
            dbconn = DriverManager.getConnection(url);
            System.out.print(dbPath);
        } catch (SQLException e) {
            System.out.println(e.getMessage()); 
        }
        return dbconn;
    }

    public DBConn() {
    }

    public int runExecuteStatement(String query) {
        int result = 0;
        Connection conn = this.getConnection();
        try{
            Statement stmt = conn.createStatement();
            result = stmt.executeUpdate(query);
            conn.close();
      }catch (SQLException e ) {
          System.out.print("Excepcion en executeStatement:" + e);
      }
      return result;
    }
    
    public ResultSet runQuery(String query) throws SQLException {
        Connection conn = this.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
}
