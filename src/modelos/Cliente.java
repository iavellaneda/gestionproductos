/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Ignacio.Avellaneda
 */
public class Cliente {
    private String apellido;
    private String nombre;
    private String domicilio;
    private String dni;

    @Override
    public String toString() {
        return "Cliente{" + "apellido=" + apellido + ", nombre=" + nombre + '}';
    }

    public Cliente(String apellido, String nombre, String domicilio, String dni) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.dni = dni;
    }

    public Cliente() {
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
    public void guardar(){
        DBConn dbConnection = new DBConn();
        String sql = String.format("INSERT INTO clientes VALUES ('%s', '%s', '%s', '%s')", this.apellido, this.nombre, this.domicilio, this.dni);
        dbConnection.runExecuteStatement(sql);
    }
    
    public void actualizar(){
        DBConn dbConnection = new DBConn();
        String sql = String.format("UPDATE clientes SET apellido='%s',nombre='%s',domicilio='%s', dni='%s' WHERE dni=%s", this.apellido, this.nombre, this.domicilio, this.dni, this.dni);
        dbConnection.runExecuteStatement(sql);
    }
    
    public void borrar(){
        DBConn dbConnection = new DBConn();
        String sql = String.format("DELETE FROM clientes WHERE dni='%s'",this.getDni());
        dbConnection.runExecuteStatement(sql);
    }
    
    public static ArrayList<Cliente> listar(){
        DBConn dbConnection = new DBConn();
        String sql = "SELECT apellido, nombre, domicilio, dni FROM clientes";
        ArrayList<Cliente> result = new ArrayList<>();
        try {
            ResultSet rsClientes = dbConnection.runQuery(sql);
            while (rsClientes.next()){
                Cliente cli = new Cliente(
                        rsClientes.getString("apellido"), 
                        rsClientes.getString("nombre"), 
                        rsClientes.getString("domicilio"), 
                        rsClientes.getString("dni")
                );
                result.add(cli);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
